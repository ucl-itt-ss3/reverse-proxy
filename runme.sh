#!/bin/bash

apt update -y
apt install nginx -y
unlink /etc/nginx/sites-enabled/default
echo "server {
        listen 80;
        location /webapp {
			proxy_pass http://172.16.0.3;
        }
    }" > /etc/nginx/sites-available/reverse-proxy.conf
echo "server {
        listen 80;
        location /nagios {
			proxy_pass http://172.16.0.4;
        }
    }" >> /etc/nginx/sites-available/reverse-proxy.conf
echo "server {
        listen 80;
        location /librenms {
			proxy_pass http://172.16.0.7;
        }
    }" >> /etc/nginx/sites-available/reverse-proxy.conf
ln -s /etc/nginx/sites-available/reverse-proxy.conf /etc/nginx/sites-enabled/reverse-proxy.conf
service nginx configtest
service nginx restart