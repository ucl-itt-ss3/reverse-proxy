# Reverse Proxy

## Table of Content
- [Current Status](https://gitlab.com/ucl-itt-ss3/reverse-proxy/tree/master#current-status)
- [Description](https://gitlab.com/ucl-itt-ss3/reverse-proxy/tree/master#description)
- [How to use](https://gitlab.com/ucl-itt-ss3/reverse-proxy/tree/master#how-to-use)
    - [Changing configuration](https://gitlab.com/ucl-itt-ss3/reverse-proxy/tree/master#changing-configuration)
    - [Change listening port](https://gitlab.com/ucl-itt-ss3/reverse-proxy/tree/master#changing-listening-port)
    - [Change Location](https://gitlab.com/ucl-itt-ss3/reverse-proxy/tree/master#changing-location)
    - [Specifying Tatget](https://gitlab.com/ucl-itt-ss3/reverse-proxy/tree/master#specifying-target)


## Current Status
 Working

## Description

This project is made to install Nginx for having a reverse proxy running on a default Debian 9.5 image.

The runme.sh will step-by-step:
- Update the repositories
- Install Nginx
- Unlink the default config file
- Add text to a config file
- Create symbolic link between two files (creating new config file nginx will use)
- Test the Nginx config
- Restart Nginx service

## How to use
**When running the runme.sh you must specify the url**

	git clone git@gitlab.com:ucl-itt-ss3/reverse-proxy.git
	cd reverse-proxy/
	bash ./runme.sh

This creates a default configuration with for listening on port 80.

## Changing configuration

### Changing listening port.
- Specifying another port is done by changing the number on the "listen" line.

#### Example
Using "listen 80;" the config will look like:

	echo "server {
        listen 80;
        location / {
			proxy_pass http://127.0.0.1/;
        }

### Changing Location
The location add another variable to look for when "listening". The location variable is simply a "/" as you normally do when browsing the web.
- The location variable requires a "/" but anything after that is fair game.

#### Example
Using "/nagios" the config will look like:

	echo "server {
        listen 80;
        location /nagios {
			proxy_pass http://127.0.0.1/;
        }

### Specifying Target
When specifying the target you use the proxy_pass variable. This contatins the IP address of the target you are reverse proxying to.
- This variable requires "http://" and will throw an error if not used.

#### Example
Using "proxy_pass http://127.0.0.1;" the config would look like:
		
	echo "server {
        listen 80;
        location /nagios {
			proxy_pass http://<IP of target>;
        }

