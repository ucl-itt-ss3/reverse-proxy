DevOps is a method of going from development to production. The process changed from getting information about bugs and errors from the continuous integration loop instead of getting the information from the users of the product.

The loop contains these elements:
- The developer starts by designing the new feature for the product.
- The developer then writes the code for the new feature.
- Then the developer runs test to checks whether or not the new code will break the existing code.

When the loop ends, the code is added to a repository where a build is attempted to be built. After the build have been made an integration test is made which checks if the code/build can be integrated into the product.
If the build is accepted, then the product can get integrated into the product.
IF the integration check ends with an error the developer gets notified more or less instantaneously.

This is contrary to the “old way” to handle this where the testing phase is happening when the code is integrated into the system and if any errors or bugs show up it can take days or even weeks before the developer gets notice.

So, the new system speeds up the whole development to operations process by making bug reporting happen earlier and enable integration into an already running system so the downtime is as low as possible.
